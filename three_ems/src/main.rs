use std::collections::HashMap;
use std::io;

fn mean(vec: &Vec<usize>) -> usize {
    vec.iter().sum::<usize>() / vec.len()
}

fn median(vec: &Vec<usize>) -> usize {
    let index = if vec.len() % 2 == 0 {
        vec.len() / 2 - 1
    } else {
        vec.len() / 2
    };
    vec[index]
}

fn mode(vec: &Vec<usize>) -> usize {
    let mut map = HashMap::new();

    for i in vec.iter() {
        *map.entry(i).or_insert(0) += 1;        
    }

    let mut count_vec: Vec<(_, _)> = map.iter().collect();
    count_vec.sort_by(|tuple1, tuple2| tuple2.1.cmp(tuple1.1));

    **count_vec[0].0
}


fn main() {
    println!("Gimme your integers.");

    let mut int_vec = vec![];

    loop { 
        let mut input = String::new();

        io::stdin().read_line(&mut input)
            .expect("Failed to read the line!");
        for x in input.split(|c| !char::is_numeric(c)) {
            match x.parse::<usize>() {
                Ok(n) => int_vec.push(n),
                Err(_) => {
                    continue;
                }
            };
        }
        if int_vec.is_empty() {
            println!("Hmmm I found no integers here. Gimme integers!")
        } else {
            int_vec.sort();
            // println!("{:?}", int_vec);
            println!("The mean is {}", mean(&int_vec));
            println!("The median is {}", median(&int_vec));
            println!("The mode is {}", mode(&int_vec));
            break;
        }
    }   
}
