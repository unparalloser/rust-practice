fn fib(n: u64) -> u64 {
    if n < 3 {
        return 1;
    } 

    return fib(n-1) + fib(n-2);
}

fn main() {
    let n = 21;
    
    if n % 100 == 11 | 12 | 13 {
        println!("The {}th Fibonacci number is {}", n, fib(n));
    } else {
        
        let remainder = &n % 10;

        match remainder {
            1 => { println!("The {}st Fibonacci number is {}", n, fib(n)); },
            2 => { println!("The {}nd Fibonacci number is {}", n, fib(n)); },
            3 => { println!("The {}rd Fibonacci number is {}", n, fib(n)); },
            _ => { println!("The {}th Fibonacci number is {}", n, fib(n)); },
        }
    }
}
