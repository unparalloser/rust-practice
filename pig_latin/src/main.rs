use std::io;

fn main() {
    println!("I am the eng-pig translator. Write down your eng!");

    let mut str_vec: Vec<String> = vec![];
    
    loop {
        let mut string = String::new();

        io::stdin().read_line(&mut string)
            .expect("Failed to read the line!");

        if string.chars().count() < string.len() {
            println!("You typed some unaccaptable symbols! 
Please, don't do it again.");
            continue;
        } else {
            
            for s in string.split(|c| !char::is_alphabetic(c)) {
                if !s.is_empty() {str_vec.push(s.to_string().to_lowercase())};
            }

            let vowels = ['a', 'e', 'o', 'i', 'u', 'y'];
            let mut pigged_vec: Vec<String> = vec![];
            
            for s in str_vec.into_iter() {
                if s.starts_with("y") {
                    let str_divided = s.split_at(1);
                    let pig = format!("{}yay", str_divided.1);
                    pigged_vec.push(pig);
                    continue;
                } else {
                    let index =
                        match s.find(|c: char| vowels.contains(&c)) {
                            Some(i) => i,
                            None => {
                                let pig = format!("{}ay", s);
                                pigged_vec.push(pig);
                                continue;
                            },
                        };
                    let str_divided = s.split_at(index);
                    match str_divided.0 {
                        "" => {
                            let pig = format!("{}way",str_divided.1);
                            pigged_vec.push(pig);
                        }
                        _ => {
                            let pig = format!("{}{}ay",
                                              str_divided.1, str_divided.0);
                            pigged_vec.push(pig);
                        }
                    }
                }
                
            }
            
            let pig_string = pigged_vec.join(" ");
            println!("{}", pig_string);
                
            break;
        }
    }
}
