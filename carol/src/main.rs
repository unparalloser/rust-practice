fn main() {
    let day =
        ["first", "second", "third", "fourth", "fifth", "sixth",
         "seventh", "eighth", "nineth", "tenth", "eleventh", "twelth"];
    
    let mut count = 0;
    
    for n in day.iter() {
        println!("");
        println!("On the {} day of christmas my true love sent to me", n);
        verse(count);
        count = count + 1;
    };
}

fn verse(n: usize) {
    
    let gift =
        ["A partridge in a pear tree", "Two turtle doves and",
         "Three french hens", "Four colly birds", "Five gold rings",
         "Six geese a-laying", "Seven swans a-swimming",
         "Eight maids a-milking", "Nine ladies dancing",
         "Ten lords a-leaping", "Eleven pipers piping",
         "Twelve drummers drumming"];
    
    if n > 0 {
        println!("{}", gift[n]);
        verse(n-1)
    } else {
        println!("{}", gift[0]);
    }
}
