use std::io;

fn main() {
    println!("Oi! Write your Fahrenheits below.");
    loop {
        let mut f = String::new();

        io::stdin().read_line(&mut f)
            .expect("Oops! Can't read the line!");

        let f: f64 = match f.trim().parse() {
            Ok(t) => t,
            Err(_) => {
                println!("Incorrect input. Now say the same, but in integers.");
                continue;
            }
        };

        let c = (f - 32.0) / 1.8;
        println!("{}F is {:.1}C.", f, c);
        break;
    };
}
