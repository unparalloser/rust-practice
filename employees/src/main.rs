use std::io;
use std::collections::HashMap;

fn main() {
    println!("Available commands:");
    println!("Add Name to Department -- adds a person to the specified department. If employee's name or surname contains 'To', please, make sure to write it with the capital 'T'.");
    println!("Department -- retrieves the list of people in a department");
    println!("All -- retrieves the list of people in the company by department");
    println!("Quit -- quit the program");
    
    let mut employees: HashMap<String, Vec<String>> = HashMap::new();

    loop {
        
        let mut input = String::new();
        
        io::stdin().read_line(&mut input)
            .expect("Failed to read the line!");

        let input_vec: Vec<&str> = input.split_whitespace().collect();

        let command = input_vec[0];

        match command {
            "Add" => {
                match input_vec.iter().position(|&x| x == "to") {
                    Some(i) => add(&input_vec, &mut employees, i),
                    None => {
                        println!("Couldn't find 'to' in the command, so I don't know what department this person is in! Please, try again.");
                        continue;
                    },
                };
                //add(&input_vec, &mut employees);
                //println!("{:?}", employees);
            },
            "All" => { 
                if employees.is_empty() {
                    println!("The list of employees is empty!");
                } else {
                    let mut key_vec = Vec::new();
                    for key in employees.keys() {
                        key_vec.push(key.clone());                     
                    }
                    key_vec.sort_by(|a, b| a.cmp(b));
                    for key in key_vec.iter() {
                        let sorted_employees = sort_employees(key, &mut employees);
                        println!("{}: {}", key, sorted_employees.join(", "));
                    };
                }
            },
            "Quit" => break,
            _ => {
                let input = input.trim();
                if employees.contains_key(input) {
                    let sorted_employees = sort_employees(&input, &mut employees);
                    println!("{}: {}", input, sorted_employees.join(", "));
                } else {
                    println!("I found no {} department!", input);
                };
            },
            
        }
    }
    
}

fn add(input: &Vec<&str>, map: &mut HashMap<String, Vec<String>>, sep_index: usize) {
    
    let employee = input[1..sep_index].join(" ");

    let department = input[sep_index + 1 ..].join(" ");

    println!("Added {} to {}", employee, department);
 
    map.entry(department).or_insert(Vec::new()).push(employee);

}

fn sort_employees(key: &str, map: &mut HashMap<String, Vec<String>>) -> Vec<String> {
    let mut employee_list = map.get_mut(key).unwrap().clone();
    employee_list.sort_by(|a, b| a.cmp(b));
    return employee_list.to_vec();
}
